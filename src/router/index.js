import Vue from 'vue'
import Router from 'vue-router'
import Nav from '@/components/Nav'
import topic from '@/components/topic'
import topicans from '@/components/topicans'
import codeedit from '@/components/codeedit'
import compilelog from '@/components/compilelog'
import border1 from '@/components/border1'
import border2 from '@/components/border2'
import border3 from '@/components/border3'

Vue.use(Router)
//url 
// http://localhost:8080/#/309-1/1?user_id=6441&class_id=2018400005&user_name=%E9%99%B3%E6%98%8E%E4%BB%81&language=JavaScriptV2

export default new Router({
  routes: [{
    path: '/index',
    components: {
      default: Nav,
      topic: topic,
      topicans: topicans,
      compilelog: compilelog,
      codeedit: codeedit,
      border1: border1,
      border2: border2,
      border3: border3
    }
  }]
})
